(function () {


    var dialogData = {
        role_imitate: '',
        role_names: {
            '1': '管理员',
            '2': '小编',
            '3': '客服'
        },
        edit_model: false,
        edit: true,
        edit_action: '',
        f: false,
        default_all: true,
        roleCheck: [1, 2],
        show: false,
        lang:{
            title:'权限设置',
            sys_role:'系统设定,不可编辑',
            f:'不选择,除了超管没有其他角色可以访问',
            f_not:'不选择,任何角色都可以访问',
            cancel:'取消',
            submit:'提交',
            imitate:'模拟',
            close:'关闭'
        }
    };
    var pageSpan = null;
    var el_current = null;

    var onRoleEdit = function (action, roles, close) {
        close();
        console.log(action + ":" + JSON.stringify(roles));
    };

    var onPageNoRole = function () {
        alert('你没有权限访问当前页面');
        history.go(-1);
    };

    function parseSearch() {
        var search_l = location.search.substr(1);
        if(!search_l)return {};
        search_l=search_l.split('&');
        var search = {};
        for (i = 0; i < search_l.length; i++) {
            var kv = search_l[i].split('=');
            search[kv[0]] = kv[1];
        }
        return search;
    }

    function hashChange() {
        if (!pageSpan)return;
        var role_key = hashToKey();
        var roleCheck = config.roles[role_key];
        if (!roleCheck) {
            roleCheck = [];
        }
        if (config.edit_model) {
            pageSpan.classList.remove('vue-role-dot-has-role');
            pageSpan.classList.remove('vue-role-dot-f');
            pageSpan.classList.remove('vue-role-dot');
            if (roleCheck.length > 0) {
                pageSpan.classList.add('vue-role-dot-has-role');
            } else {
                pageSpan.classList.add('vue-role-dot');
            }
        }
        if (roleCheck.length > 0) {
            if (config.role == config.super_role) {
                return;
            }
            for (i = 0; i < roleCheck.length; i++) {
                if (roleCheck[i] == config.role) {
                    return;
                }
            }
            onPageNoRole();
        }
    }

    window.addEventListener("popstate", hashChange, false);

   var vueRoleEditor=  Vue.component('vue-role-dialog', {
        template: '<div v-if="edit_model">' +
        '               <div v-if="show"  class="vue-role-dialog__wrapper" >' +
        '                   <div class="vue-role-dialog" >' +
        '                       <div class="vue-role-dialog__header" >' +
        '                           <div >' +
        '                               <span>{{lang.title}}</span>' +
        '                           </div>' +
        '                       </div>' +
        '                       <div class="vue-role-dialog__content" style="">' +
        '                          <span style="width:120px;display: inline-block;padding-right: 10px;" v-for="(v,k) in role_names"> <input type="checkbox" :disabled="!edit" v-model="roleCheck" :value="k" /> {{v}}</span>' +
        '                          <div v-show="!edit" class="info"><span>{{lang.sys_role}}</span></div>' +
        '                          <div v-show="edit" class="info"><span>{{f? lang.f:lang.f_not}}</span></div>' +
        '                       </div>' +
        '                       <div class="vue-role-dialog__btns" >' +
        '                           <button @click="show=false" class="vue-role-btn"  type="button" >' +
        '                               <span>{{lang.cancel}}</span>' +
        '                           </button>' +
        '                           <button @click="saveRole" class="vue-role-btn vue-role-btn-primary" type="button" >' +
        '                               <span>{{lang.submit}}</span>' +
        '                           </button>' +
        '                           </div>' +
        '                       </div>' +
        '                   </div>' +
        '                   <div v-if="show" class="vue-role-model"  tabindex="0" style="z-index: 2000;"></div>' +
        '                   <div class="vue-role-change">' +
        '                       <select  v-model="role_imitate">' +
        '                           <option  v-for="(v,k) in role_names" :value="k"> {{v}}</option>' +
        '                       </select>' +
        '                       <button @click="imitate" style="border-radius:0"  class="vue-role-btn vue-role-btn-primary">{{lang.imitate}}</button>' +
        '                       <button @click="close" style="border-radius:0"  class="vue-role-btn ">{{lang.close}}</button>' +
        '                   </div>' +
        '           </div>',
        data: function () {
            return dialogData;
        }, methods: {
            imitate: function () {
                var search = parseSearch();
                search.vue_role_edit_model = 1;
                search.vue_role_imitate = this.role_imitate;
                var m = '';
                for (var k in  search) {
                    if (!search.hasOwnProperty(k)) {
                        return;
                    }
                    if (m) {
                        m += '&';
                    }
                    m += k + '=' + search[k];
                }
                location.href = location.pathname + '?' + m + location.hash;
            }, close: function () {
                var search = parseSearch();
                delete search.vue_role_edit_model;
                delete search.vue_role_imitate;
                var m = '';
                for (var k in  search) {
                    if (!search.hasOwnProperty(k)) {
                        return;
                    }
                    if (m) {
                        m += '&';
                    }
                    m += k + '=' + search[k];
                }
                location.href = location.pathname + '?' + m + location.hash;
            },
            saveRole: function () {
                var me = this;

                function close() {
                    me.show = false;
                }

                if (!this.edit) {
                    close();
                } else {
                    config.roles[this.edit_action] = this.roleCheck;
                    el_current.classList.remove('vue-role-dot-has-role');
                    el_current.classList.remove('vue-role-dot-f');
                    el_current.classList.remove('vue-role-dot');
                    if (this.roleCheck.length > 0) {
                        el_current.classList.add('vue-role-dot-has-role');
                    } else {
                        if (this.f) {
                            el_current.classList.add('vue-role-dot-f');
                        } else {
                            el_current.classList.add('vue-role-dot');
                        }
                    }
                    onRoleEdit(this.edit_action, this.roleCheck, close);
                }
            }
        }
    });
    var vueRoleEditorDiv = document.createElement('div');
    vueRoleEditorDiv.id='vue-role-editor';
    document.body.appendChild(vueRoleEditorDiv);
    var editor=(new vueRoleEditor);
    editor.$mount('#vue-role-editor');
    function hashToKey() {
        var hash = window.location.hash;
        hash = hash.substr(hash.indexOf('/') == 1 ? 2 : 1);
        if (hash.indexOf('?') > -1) {
            hash = hash.substr(0, hash.indexOf('?'));
        }
        var path = hash.split('/');
        var m = [];
        for (var i = 0; i < path.length; i++) {
            var item = path[i];
            if (!(parseInt(item) + "" == item || item.indexOf('@') == 0)) {
                m.push(item);
            }
        }
        path = m;
        return path.join('/');
    }

    var config = {
        edit_model: false,
        role: 1,
        super_role: 1,
        super: true,
        roles: {
            'customer.menu': []//管理
        },
        role_names: {
            '1': '管理员',
            '2': '小编',
            '3': '客服'
        },
        lang:{
            title:'权限设置',
            sys_role:'系统设定,不可编辑',
            f:'不选择,除了超管没有其他角色可以访问',
            f_not:'不选择,任何角色都可以访问',
            cancel:'取消',
            submit:'提交',
            imitate:'模拟',
            close:'关闭'
        }
    };

    window.VueRole = {
        config: function (set) {
            var me=this;
            for (var i in set) {
                if (!set.hasOwnProperty(i)) {
                    return;
                }
                config[i] = set[i];
            }
            config.role = "" + config.role;
            config.super_role = "" + config.super_role;
            config.super = config.role == config.super_role;
            var search = parseSearch();
            config.edit_model = search['vue_role_edit_model'];
            dialogData.edit_model = config.edit_model;
            var role = search['vue_role_imitate'];
            if (role) {
                config.role = role;
                config.super = false;
            }
            dialogData.role_imitate = config.role;
            dialogData.role_names = config.role_names;
            if (config.edit_model) {
                pageSpan = document.createElement('span');
                pageSpan.classList.add('vue-role-page');
                pageSpan.classList.add('vue-role-dot');
                document.body.appendChild(pageSpan);
                pageSpan.onclick = function (event) {
                    el_current = pageSpan;
                    event.stopPropagation();
                    dialogData.show = true;
                    dialogData.f = false;
                    var role_key = hashToKey();
                    var roles = config.roles[role_key];
                    if (!roles) {
                        roles = [];
                    }
                    dialogData.roleCheck = roles;
                    dialogData.edit = true;
                    dialogData.edit_action = role_key;
                }
            }
            hashChange();
            if (config.role == config.super_role) {
                document.onkeydown = function (e) {
                    if (e.ctrlKey && e.key == '/') {
                        me.editModel(!config.edit_model);
                    }
                };
            }
        },
        editModel:function (open) {
            if(open==null)open=true;
            var search = parseSearch();
            delete search.vue_role_edit_model;
            delete search.vue_role_imitate;
            if(open==true){
                search.vue_role_edit_model = 1;
            }
            var m = '';
            for (var k in  search) {
                if (!search.hasOwnProperty(k)) {
                    return;
                }
                if (m) {
                    m += '&';
                }
                m += k + '=' + search[k];
            }
            location.href = location.pathname + '?' + m + location.hash;
        },
        onRoleEdit: function (fun) {
            onRoleEdit = fun;
        },
        onPageNoRole: function (fun) {
            onPageNoRole = fun;
        }
    };

    // recursively search for possible transition defined inside the component root
    function locateNode (vnode) {
        return vnode.componentInstance && (!vnode.data || !vnode.data.transition)
            ? locateNode(vnode.componentInstance._vnode)
            : vnode
    }

    Vue.directive('role', {
        inserted: function (el, binding,vnode) {
            var role_key=binding.rawName.replace('v-role:','').replace('.$f','').replace('.$h','');
            var arg = binding.arg;
            if (config.edit_model&&!binding.modifiers.$h&&(!binding.expression||binding.value)) {
                if (!el.style.position || el.style.position == 'inherit' || el.style.position == 'initial' || el.style.position == 'unset' || el.style.position == 'static') {
                    el.style.position = 'relative';
                }
                var span = document.createElement('span');
                var roles = config.roles[role_key];
                if (!roles) {
                    roles = [];
                }
                if (roles.length > 0||!arg) {
                    span.classList.add('vue-role-dot-has-role');
                } else {
                    if (binding.modifiers.$f) {
                        span.classList.add('vue-role-dot-f');
                    } else {
                        span.classList.add('vue-role-dot');
                    }
                }
                span.onclick = function (event) {
                    el_current = span;
                    event.stopPropagation();
                    dialogData.show = true;
                    dialogData.f = binding.modifiers.$f;
                    if (!arg) {
                        var keys = [];
                        for (var i in binding.modifiers) {
                            if (binding.modifiers.hasOwnProperty(i)) {
                                if (i == '$f') {
                                    f = true;
                                }else
                                if (i == '$h') {
                                    h = true;
                                }else {
                                    keys.push(i)
                                }
                            }
                        }
                        dialogData.roleCheck = keys;
                        dialogData.edit = false;
                        dialogData.edit_action = '';
                    } else {
                        dialogData.roleCheck = roles;
                        dialogData.edit = true;
                        dialogData.edit_action = role_key;
                    }
                };
                el.appendChild(span);
                span = document.createElement('span');
                span.style.clear = 'both';
                el.appendChild(span);
            }
            if (config.role == config.super_role) {
                return;
            }
            if (!arg) {
                //没有设置action
                if (binding.modifiers[config.role]) {
                    return;
                }
            } else {
                if (!config.roles[role_key] || config.roles[role_key].length == 0) {
                    if (!binding.modifiers.$f) {
                        return;
                    }
                } else {
                    for (i = 0; i < config.roles[role_key].length; i++) {
                        if (config.roles[role_key][i] == config.role) {
                            return;
                        }
                    }
                }
            }
            el.remove();
            el.style.display='none';
        }
    });


})();



