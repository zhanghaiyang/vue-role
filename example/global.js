(function () {
    Rap.ready(function () {

        //测试时直接权限直接存在本地缓存 localStorage中
        var roles = {};
        for (var i = 0; i < localStorage.length; i++) {
            var k = localStorage.key(i);
            if (k.indexOf('vue_role@') == 0) {
                var value = localStorage.getItem(k);
                k = k.substr('vue_role@'.length);
                roles[k] = JSON.parse(value);
            }
        }

        //配置
        VueRole.config({
            role: 1,        //当前用户的 权限
            super_role: 1,  //超级用户的权限
            roles: roles,    //权限数据
            role_names: {'1': '超管', '2': '技术', '3': '小编'} //权限名称
        });

        VueRole.onRoleEdit(function (action, roles, close) {
            //测试时直接权限直接存在本地缓存 localStorage中
            localStorage.setItem('vue_role@' + action, JSON.stringify(roles));
            close();
        });


    }).then(function (rs) {

    }).then(function () {
        window.App = Rap.app({
            el: '#app',
            data: function () {
                return {
                    defaultMenu: '',
                    isCollapse: false,
                    admin_user: '',
                    menuList: {},
                    actions: ''
                };
            }, created: function () {
                var me = this;
                setTimeout(function () {
                    var page = me.router.page.split('/');
                    var menu = page[0];
                    me.defaultMenu = menu;
                }, 100)

            }, init: function () {

            }, methods: {
                menuClick: function (index) {
                    var me = this;
                    index = index + '/index'
                    Rap.go(index);
                }
            }
        });
        // alert(');
    }).catch(function (e) {
        console.log(e);
    });


    function resize() {
        var app = document.getElementById('app');
        app.style.height = (window.innerHeight - 40) + "px";
    }

    window.addEventListener('resize', resize);
    resize();
})();